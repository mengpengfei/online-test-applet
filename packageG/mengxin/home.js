const app = getApp()
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    elements: [{
        title: '调研',
        name: 'survey',
        color: 'cyan',
        icon: 'newsfill'
      },
      {
        title: '设计院',
        name: 'designing',
        color: 'blue',
        icon: 'colorlens'
      },
      {
        title: '方案',
        name: 'scheme',
        color: 'purple',
        icon: 'font'
      },
      {
        title: '交流会 ',
        name: 'meeting',
        color: 'mauve',
        icon: 'icon'
      },
      {
        title: '招投标',
        name: 'bidding',
        color: 'pink',
        icon: 'btn'
      },
      {
        title: '基础知识',
        name: 'knowledge',
        color: 'brown',
        icon: 'tagfill'
      },
    ],
  },

})