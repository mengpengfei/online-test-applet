const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    BigScreen:null,
    long: null,
    high: null,
    index1: null,
    index2: null,
    index3: 0,
    multiIndex: [0, 0],
    picker: ['12块', '15块', '18块'],
    picker1: ['二供系统', '智慧水务系统'],
    picker3: ['尽量标准化', '根据实际尺寸'],
    picker2: ['2台', '3台', '4台', '5台'],
    multiArray: [
      ['46寸大屏', '55寸大屏'],
      ['3.5mm拼缝', '1.8mm拼缝']
    ],
    objectMultiArray: [
      [{
          id: 0,
          name: '46寸大屏'
        },
        {
          id: 1,
          name: '55寸大屏'
        }
      ],
      [{
          id: 0,
          name: '3.5mm拼缝'
        },
        {
          id: 1,
          name: '1.8mm拼缝'
        }
      ]
    ],
    imgList: [],
    modalName: null,
    textareaAValue: '',
    textareaBValue: ''
  },
  formSubmit: function (e) {
    console.log(e)
  },
  PickerChange(e) {
    console.log(e);
    this.setData({
      index: e.detail.value
    })
  },
  PickerChange1(e) {
    console.log(e);
    this.setData({
      index1: e.detail.value
    })
  },
  PickerChange2(e) {
    console.log(e);
    this.setData({
      index2: e.detail.value
    })
  },
  PickerChange3(e) {
    console.log(e);
    this.setData({
      index3: e.detail.value
    })
  },
  MultiChange(e) {
    this.setData({
      multiIndex: e.detail.value
    })
  },
  longInput(e) {
    console.log(e);
    this.setData({
      long: e.detail.value
    })
  },
  highInput(e) {
    console.log(e);
    this.setData({
      high: e.detail.value
    })
  },
  submit(e){
    if(this.data.long<=1022||this.data.high<=576||this.data.index1==null||this.data.index2==null){
      wx.showModal({
        title: "信息填写错误提示",
        content:
          "中控室「长」或者「高」低于最小值,或者参数填写不完整," +
          "请确认后点击【我已知晓】。",
        confirmText: "我已知晓",
        confirmColor: "#4B6DE9",
        cancelText: "我拒绝",
        success: result => {
          if (!result.confirm) {
           wx.navigateBack({
             delta: 0,
           })
          } else {
            this.setData({
              high:0,
              long:0
            })
            return false;
          }
        }
      });
    }else{
    //46寸
    if(this.data.multiIndex[0]==0){
      this.setData({
        'BigScreen.high':parseInt((this.data.high-600)/576.6),
        'BigScreen.length':parseInt((this.data.long)/1022.08),
        'BigScreen.computer':parseInt(this.data.index2)+2,
        'BigScreen.economy':parseInt(this.data.index1),
        'BigScreen.frame':this.data.multiIndex[1],
        'BigScreen.category':this.data.multiIndex[0]
      })
    }else{
      this.setData({
        'BigScreen.high':parseInt((this.data.high-600)/686.1),
        'BigScreen.length':parseInt((this.data.long)/1215.30),
        'BigScreen.computer':parseInt(this.data.index2)+2,
        'BigScreen.economy':parseInt(this.data.index1),
        'BigScreen.frame':this.data.multiIndex[1],
        'BigScreen.category':this.data.multiIndex[0]
      })
    }
    //套用标准化，尽量使用12、15、18这两种组合
    if(this.data.index3==0){
        if(this.data.high>3){
          this.setData({
            high:3
          })
          if(this.data.long>6){
            this.setData({
              long:6
            })
          }
        }
    }
    app.formPost('/api/wx/student/ergong/pageList', this.data.BigScreen)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          certList: res.response.list,
          total:res.response.total,
          isLoading: false
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
    }
  },
  MultiColumnChange(e) {
    let data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    this.setData(data);
    console.log(data)
  },
  TimeChange(e) {
    this.setData({
      time: e.detail.value
    })
  },
  DateChange(e) {
    this.setData({
      date: e.detail.value
    })
  },
  RegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  },
  ChooseImage() {
    wx.chooseImage({
      count: 4, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.imgList.length != 0) {
          this.setData({
            imgList: this.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
  textareaAInput(e) {
    this.setData({
      textareaAValue: e.detail.value
    })
  },
  textareaBInput(e) {
    this.setData({
      textareaBValue: e.detail.value
    })
  }
})