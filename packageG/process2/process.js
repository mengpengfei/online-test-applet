const app = getApp();
Page({
  data: {
    opacity: '.3', //字体透明度
    color2: '#081EF', //字体颜色
    number: 6, //水印数量
    deg: '-45', //旋转角度
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    wenDangList: [],
    neirong: null,
    modalName: null
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  downloadfile() {
    var that = this;
    const url = that.data.wenDangList.url
    const dept = that.data.wenDangList.dept
    const password = that.data.wenDangList.password
    if (that.data.wenDangList.password) {
      wx.setClipboardData({
        data: "网址：" + url + "    账号：" + dept + "    密码：" + password,
        success(res) {
          wx.getClipboardData({
            success(res) {
              console.log(res.data) // data
              wx.showToast({
                title: '复制成功,粘贴到浏览器访问',
                icon: 'none'
              })
            }
          })
        }
      })
    } else {
      wx.showLoading({
        title: '正在下载中，请稍等！',
      })

      const downloadTask = wx.downloadFile({
        url: url,
        success: function (res) {
          wx.hideLoading()
          var filePath = res.tempFilePath;
          console.log(filePath);
          wx.openDocument({
            filePath: filePath,
            success: function (res) {
              console.log('打开文档成功')
            },
            fail: function (res) {
              console.log(res);
            },
            complete: function (res) {
              console.log(res);
            }
          })
        },
        fail: function (res) {
          console.log('文件下载失败');
        },
        complete: function (res) {},
      })
      downloadTask.onProgressUpdate((res) => {
        wx.showLoading({
          title: '下载进度' + res.progress+'%',
        })
        if(res.progress==100){
         wx.hideLoading()
        }
        console.log('下载进度' + res.progress);
        console.log('已经下载的数据长度' + res.totalBytesWritten);
        console.log('预期需要下载的数据总长度' + res.totalBytesExpectedToWrite);
      })

    }
  },
  downloadfile() {
    var that = this;
    const url = that.data.wenDangList.url
    const dept = that.data.wenDangList.dept
    const password = that.data.wenDangList.password
      wx.showLoading({
        title: '正在下载中!',
      })

      const downloadTask = wx.downloadFile({
        url: url,
        success: function (res) {
          wx.hideLoading()
          var filePath = res.tempFilePath;
          console.log(filePath);
          wx.openDocument({
            filePath: filePath,
            success: function (res) {
              console.log('打开文档成功')
            },
            fail: function (res) {
              console.log(res);
            },
            complete: function (res) {
              console.log(res);
            }
          })
        },
        fail: function (res) {
          console.log('文件下载失败');
        },
        complete: function (res) {},
      })
      downloadTask.onProgressUpdate((res) => {
        wx.showLoading({
          title: '下载进度' + res.progress+'%',
        })
        if(res.progress==100){
         wx.hideLoading()
        }
        console.log('下载进度' + res.progress);
        console.log('已经下载的数据长度' + res.totalBytesWritten);
        console.log('预期需要下载的数据总长度' + res.totalBytesExpectedToWrite);
      })
    
  },
  goToHook(e) {
    var that = this;
    const url = that.data.wenDangList.url
    // console.log(url)
    // that.setData({
    //   modalName: "Modal",
    //   neirong: url
    // });
    wx.setClipboardData({
      data: url,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功,粘贴到浏览器访问',
              icon: 'none'
            })
          }
        })
      }
    })
  },
  onLoad: function (options) {
    console.log(options)
    app.formPost('/api/wx/student/qualification/select/' + options.id, null)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            wenDangList: res.response
          })
          wx.showToast({
            title: '已扣除2积分！',
            icon: 'none',
            duration: 2000
            })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
  },
  goDownLoad: function () {
    let that = this
    app.globalData.wenDangList = that.data.wenDangList,
      wx.navigateTo({
        url: '/packageF/lockscreen/lockscreen?id=' + that.data.wenDangList.id
      })
  },
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 文档详情',
      path: '/packageG/process1/process'
    }
  },
});