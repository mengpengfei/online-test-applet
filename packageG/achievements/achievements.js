// miniprogram/pages/achievements/achievements.js
const app=getApp();

Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    avatar: null,
    queryParam1: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"培训材料"
    },
    queryParam3: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"百问百答"
    },
    queryParam4: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      content:"培训视频"
    },
    peixuncishu:0,
    baiwenbaida:0,
    peixunshipin:0,
    badgeName: ["社交达人", "笔耕不辍", "知无不言"],  //徽章名字
    content: ["培训资料", "培训视频", "百问百答"],  //徽章内容
    imgSrc: ['../image/badge-fp.png', '../image/badge-diary.png', '../image/badge-guide.png'],
    number: [null, null, null],  //发布的数量
    gap: [null, null, null],  //升级需要的篇数    
    className: ["badge-left", "badge-right", "badge-left"],  //css class名，用于排版
    levelSrc: "../image/lv1.png",
    levelSrc1:"../image/lv1.png",
    levelSrc2:"../image/lv1.png",
    fontColor: ["#791D2C", "#8376A2", "#336859"]
  },

  onLoad: function (options) {
    var _this=this;

    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam1).then(res => {
      if (res.code === 1) {
        _this.setData({
          peixuncishu:res.response.total,
        });
        if (res.response.total<100) {
          _this.setData({
           levelSrc:"../image/lv1.png"
          })
        }
        if (res.response.total>=100&&res.response.total<200) {
          _this.setData({
           levelSrc:"../image/lv2.png"
         })
       }
       if (res.response.total>200) {
        _this.setData({
           levelSrc:"../image/lv3.png"
         })
       }
      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam3).then(res => {
      if (res.code === 1) {
        _this.setData({
          baiwenbaida:res.response.total,
        });
        if (res.response.total<100) {
          _this.setData({
           levelSrc1:"../image/lv1.png"
          })
        }
        if (res.response.total>=100&&res.response.total<200) {
          _this.setData({
           levelSrc1:"../image/lv2.png"
         })
       }
       if (res.response.total>200) {
        _this.setData({
           levelSrc1:"../image/lv3.png"
         })
       }
      }
    })
    app.formPost('/api/wx/student/user/log/pageList',_this.data.queryParam4).then(res => {
      if (res.code === 1) {
        _this.setData({
          peixunshipin:res.response.total,
        });
        if (res.response.total<100) {
          _this.setData({
           levelSrc2:"../image/lv1.png"
          })
        }
        if (res.response.total>=100&&res.response.total<200) {
          _this.setData({
           levelSrc2:"../image/lv2.png"
         })
       }
       if (res.response.total>200) {
        _this.setData({
           levelSrc2:"../image/lv3.png"
         })
       }
      }
    })
    // 获取数量，计算等级
    this.loadData()
  },

  loadData: function(){

  if (this.data.peixunshipin<10) {
    this.setData({
     levelSrc1:"../image/lv1.png"
    })
  }
  if (this.data.peixunshipin>=10&&this.data.peixunshipin<50) {
   this.setData({
     levelSrc1:"../image/lv2.png"
   })
 }
 if (this.data.peixunshipin>50) {
   this.setData({
     levelSrc1:"../image/lv3.png"
   })
 }

 if (this.data.baiwenbaida<10) {
  this.setData({
   levelSrc2:"../image/lv1.png"
  })
}
if (this.data.baiwenbaida>=10&&this.data.baiwenbaida<50) {
 this.setData({
   levelSrc2:"../image/lv2.png"
 })
}
if (this.data.baiwenbaida>50) {
 this.setData({
   levelSrc2:"../image/lv3.png"
 })
}

  },
})