// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    opacity:'.3',//字体透明度
    color2:'#081EF',//字体颜色
    number:6,//水印数量
    deg:'-45',//旋转角度
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    isLoading: true,
    userInfo: null,
    modalName: null,
    neirong: null,
    total:0,
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    certList: [],
    certList1: [],
    certList2: [],
    certList3: [],
    orderby: '培训材料-漏损控制',
    orderbyList: [{
      title: '漏损控制',
      value: '培训材料-漏损控制'
    }, {
      title: '智慧供水',
      value: '培训材料-智慧供水'
    }, {
      title: '基础知识',
      value: '培训材料-基础知识'
    }, {
      title: '产品交付',
      value: '培训材料-产品交付'
    }],
    queryParam: {
      pageIndex: "1",
      pageSize: "1000",
      name: "培训材料-漏损控制",
      title:""
    },
  },

  keywordInput: function (e) {
    this.setData({
      'queryParam.title': e.detail.value
    });
  },
  searchBooks:function (params) {
    this.getHotBooksData(this.data.orderby)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  inital: function (orderby) {
    this.setData({
     'queryParam.name': orderby
       });
  },
  orderbyChanged: function (e) {
    const orderby = this.data.orderbyList[e.currentTarget.dataset.index].value;
    this.setData({ orderby: orderby});
    this.getHotBooksData(orderby);
  },
  getHotBooksData: function (orderby) {
    this.inital(orderby)
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          certList: res.response.list,
          total:res.response.total,
          isLoading: false
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
  },

  onLoad: function (options) {
    const orderby = options.orderby ? options.orderby : '培训材料-漏损控制';
    this.setData({
      userInfo: app.globalData.userInfo
    })
    this.inital(orderby);
    this.getHotBooksData(orderby);
    this.initall();
  },
  initall: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    wx.showModal({
      title: "积分信息提示",
      content:
        "点击查看需要消耗您的积分，" +
        "请确认后点击【我已知晓】。",
      confirmText: "我已知晓",
      confirmColor: "#4B6DE9",
      cancelText: "我拒绝",
      success: result => {
        if (!result.confirm) {
         wx.navigateBack({
           delta: 0,
         })
        } else {
          wx.navigateTo({
            url: '../process/process?id=' + id,
        });
        }
      }
    });

  },
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 培训资料',
      path: '/packageG/book/book'
    }
  },
})