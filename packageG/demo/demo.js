const app = getApp();
Page({
  data: {
    opacity:'.3',//字体透明度
    color2:'#081EF',//字体颜色
    number:6,//水印数量
    deg:'-45',//旋转角度
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    neirong: null,
    userInfo:null,
    queryParam: {
      pageIndex: "1",
      pageSize: "1000",
      name: "演示系统"
    },
    showLogin: false,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    color1:'#9a9a9a',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    wenDangList:null,
    modalName:null
  },
    //页面滚动执行方式
    onPageScroll(e) {
      this.setData({
        scrollTop: e.scrollTop
      })
    },
    onLoad(){
      this.setData({
        userInfo: app.globalData.userInfo
      })
       this.search()
    },
    goToHook(e) {
      const id = e.currentTarget.dataset.id
      wx.showModal({
        title: "积分信息提示",
        content:
          "点击查看需要消耗您的积分，" +
          "请确认后点击【我已知晓】。",
        confirmText: "我已知晓",
        confirmColor: "#4B6DE9",
        cancelText: "我拒绝",
        success: result => {
          if (!result.confirm) {
           wx.navigateBack({
             delta: 0,
           })
          } else {
            wx.navigateTo({
              url: '../process/process?id=' + id,
          });
          }
        }
      });
    },
    // goToHook1(e) {
    //   var that = this;
    //   const id = e.currentTarget.dataset.id
    //   console.log(id)
    //   app.formPost('/api/wx/student/book/select/' + id, null)
    //   .then(res => {
    //     if (res.code == 1) {
    //       that.setData({
    //         wenDangList: res.response,
    //         modalName:"Modal"
    //       })
    //       wx.setClipboardData({
    //         data: "网址："+res.response.url+"   账号："+res.response.dept+"   密码："+res.response.password,
    //         success(res) {
    //           wx.getClipboardData({
    //             success(res) {
    //               console.log(res.data) // data
    //               wx.showToast({
    //                 title: '复制成功,已扣除2积分，粘贴到浏览器访问',
    //                 icon: 'none',
    //                 duration: 2000
    //                 })
    //             }
    //           })
    //         }
    //       })
    //     } else {
    //       wx.showModal({
    //         title: res.message
    //       })
    //     }
    //   }).catch(e => {
    //     wx.showModal({
    //       title: e
    //     })
    //   })

    // },
  search: function () {
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          certList: res.response.list
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  bindSearchInput: function (e) {
    this.setData({
      'queryParam.title': e.detail.value,
      'queryParam.name': "演示系统"
    });
    // this.setData({
    //   queryParam: {
    //     pageIndex: 1,
    //     pageSize: app.globalData.pageSize,
    //     title: e.detail.value,
    //     name: "演示系统"
    //   },
    // })
    // this.search();
    // return e.detail.value;
  },
});