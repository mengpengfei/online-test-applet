const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    bottom: 100,
    bgColor2: "#5677fc",
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    neirong: null,
    userInfo: null,
    queryParam: {
      pageIndex: "1",
      pageSize: "100"
    },
    showLogin: false,
    spinShow: false,
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    color1: '#9a9a9a', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    userInfo: [],
    shuLiang: null,
    showLogin1:false,
    bgColor1:'#f37b1d',
    color1:'#39b54a',
    swiperList: [{
      id: 0,
      type: 'image',
      url: 'https://z3.ax1x.com/2021/10/19/5wWM0f.jpg'
    },
    {
      id: 1,
      type: 'image',
      url: 'https://z3.ax1x.com/2021/10/19/5wW1AS.jpg'
    },
    {
      id: 2,
      type: 'image',
      url: 'https://z3.ax1x.com/2021/10/19/5wWKnP.jpg'
    },
    {
      id: 3,
      type: 'image',
      url: 'https://z3.ax1x.com/2021/10/19/5wWQ78.jpg'
    },
    {
      id: 4,
      type: 'image',
      url: 'https://z3.ax1x.com/2021/10/19/5wW3tg.jpg'
    },
    {
      id: 5,
      type: 'image',
      url: 'https://z3.ax1x.com/2021/10/19/5wW8hQ.jpg'
    }
  ],
    categories: [{
        cuIcon: 'list',
        color: 'red',
        badge: '100',
        mid: '1',
        name: '培训资料'
      }, {
        cuIcon: 'goods',
        color: 'orange',
        badge: 100,
        mid: '2',
        name: '优秀方案'
      }, {
        cuIcon: 'redpacket',
        color: 'yellow',
        badge: 20,
        mid: '3',
        name: '报价文件'
      }, {
        cuIcon: 'comment',
        color: 'cyan',
        badge: 100,
        mid: '4',
        name: '百问百答'
      },
      {
        cuIcon: 'hotfill',
        color: 'purple',
        badge: 24,
        mid: '5',
        name: '演示系统'
      },
      {
        cuIcon: 'magic',
        color: 'mauve',
        badge: 10,
        mid: '6',
        name: '招标投标'
      },
      {
        cuIcon: 'friend',
        color: 'black',
        badge: 16,
        mid: '7',
        name: '竞争对手'
      },
      {
        cuIcon: 'lightfill',
        color: 'pink',
        badge: 43,
        mid: '8',
        name: '标准化资料'
      },
      {
        cuIcon: 'camera',
        color: 'green',
        badge: 23,
        mid: '9',
        name: '案例分享'
      },
      // {
      //   cuIcon: 'discover',
      //   color: 'blue',
      //   badge: 195,
      //   mid: '10',
      //   name: '物料中心'
      // },
      {
        cuIcon: 'activity',
        color: 'blue',
        badge: 16,
        mid: '11',
        name: '产品手册'
      },
      // {
      //   cuIcon: 'square',
      //   color: 'mauve',
      //   badge: 2,
      //   mid: '12',
      //   name: '大屏计算工具'
      // }
    ],
    cardCur: 0,
    curriculum: [{}],
    projectList: []
  },
  cardSwiper: function cardSwiper(e) {
    this.setData({
      cardCur:e.detail.current
    })
  },
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  gotoVideo(e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/packageG/shipin/shipin?id=' + id,
    });
  },
  tomore() {
    wx.navigateTo({
      url: '/packageG/cart2/cart',
    });
  },
  gotoStudy(e) {
    let id = e.currentTarget.dataset.mid;
    if (id == "1") {
      wx.navigateTo({
        url: '/packageG/book/book',
      });
    }
    if (id == "2") {
      wx.navigateTo({
        url: '/packageG/project/project',
      });
    }
    if (id == "3") {
      wx.navigateTo({
        url: '/packageG/cert/cert',
      });
    }
    if (id == "4") {
      wx.navigateTo({
        url: '/packageG/wenDa/wenDa',
      });
    }
    if (id == "5") {
      wx.navigateTo({
        url: '/packageG/demo/demo',
      });
    }
    if (id == "6") {
      wx.navigateTo({
        url: '/packageG/toubiao/toubiao',
      });
    }
    if (id == "7") {
      wx.navigateTo({
        url: '/packageG/friend/friend',
      });
    }
    if (id == "8") {
      wx.navigateTo({
        url: '/packageG/standard/standard',
      });
    }
      if (id == "9") {
        wx.navigateTo({
        url: '/packageG/anli/anli',
      });
      }
      if (id == "10") {
        wx.navigateTo({
          url: '/packageG/wuliao/wuliao',
        });
      }
      if (id == "11") {
        wx.navigateTo({
          url: '/packageG/qualification/qualification',
        });
      }
      if (id == "12") {
        wx.navigateTo({
          url: '/packageG/daping/daping',
        });
      }
  },
  onLoad(e) {

    this.setData({
      spinShow: true,
      userInfo: app.globalData.userInfo
    });
    this.search()
    if(this.data.userInfo.phone==null||this.data.userInfo.phone==""){
      this.setData({
        showLogin1: true
      })
    }
  },
  jiandao(e) {
    const category = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/packageG/cart1/cart?category=' + category,
    })
  },
  mengxin(e){
    wx.navigateTo({
      url: '/packageG/mengxin/home',
    })
  },
  feature() {
    this.setData({
      showLogin: true
    })
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../process1/process?id=' + id,
    });
  },
  onPullDownRefresh() {
    this.setData({
      spinShow: true
    });
    if (!this.loading) {
      this.search()
      wx.showToast({
        title: '刷新成功',
        icon: "none"
      })
    }
  },
  search: function () {
    app.formPost('/api/wx/student/video/pageList', this.data.queryParam)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            curriculum: res.response.list,
            spinShow: false
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
  }
});