Component({
    properties: {
        question: {
            type: Object,
            value: {}
        },
        answer: {
            type: Object,
            value: {}
        },
        qType : {
            type: Number,
            value: 0
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
    },

    /**
     * 组件的方法列表
     */
    methods: {
    }
})
