//index.js
//获取应用实例
const app = getApp()
const jinrishici = require('../../utils/jinrishici.js')
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    showLogin1:false,
    bgColor1:'#f37b1d',
    color1:'#39b54a',
    userInfo: app.globalData.userInfo,
    iconList: [{
      icon: 'formfill',
      color: 'red',
      badge: 0,
      name: '试卷中心',
      url: '/packageF/exam/index/index'
    }, {
      icon: 'newsfill',
      color: 'orange',
      badge: 1,
      name: '考试中心',
      url: '/packageF/examPapers1/index'
    }, {
      icon: 'picfill',
      color: 'yellow',
      badge: 0,
      name: '成绩查询',
      url: '/packageF/search/search'
    }, {
      icon: 'markfill',
      color: 'olive',
      badge: 0,
      name: '考试记录',
      url: '/packageF/record/index'
    }, {
      icon: 'samefill',
      color: 'cyan',
      badge: 0,
      name: '题目搜索',
      url:'/packageF/question/question'
    }, {
      icon: 'upstagefill',
      color: 'blue',
      badge: 0,
      name: '成绩排名',
      url: "/packageF/rank/index"
    }, {
      icon: 'medalfill',
      color: 'purple',
      badge: 0,
      name: '我的证书',
      url: "/packageF/certificateList/index"
    }, {
      icon: 'questionfill',
      color: 'mauve',
      badge: 0,
      name: '考试分析',
      url: "/packageF/history/index"
    }, {
      icon: 'commandfill',
      color: 'purple',
      badge: 0,
      name: '地图定位',
      url: "/packageF/map/index"
    }, {
      icon: 'command',
      color: 'purple',
      badge: 1,
      name: '地图定位pro',
      url: "/packageF/mapList/index"
    },
    {
      icon: 'brandfill',
      color: 'mauve',
      badge: 0,
      name: '考试日历',
      url: "/packageF/calendar/calendar"
    }
    , {
      icon: 'shop',
      color: 'black',
      badge: 0,
      name: '班级管理',
      url: "/packageF/classesList/index"
    }, {
      icon: 'cardboard',
      color: 'red',
      badge: 0,
      name: '错题集',
      url: "/packageF/cardboard/index"
    }],
    gridCol:3,
    skin: false,
    spinShow: false,
    fixedPaperCount: 0,
    taskPaperCount: 0,
    timeLimitPaperCount: 0,
    pushPaperCount: 0,
    shici: '长风破浪会有时，直挂云帆济沧海。',
    fixedPaper: [],
    pushPaper: [],
    timeLimitPaper: [],
    taskList: [],
    msgList: []
  },
  onLoad: function () {
    this.setData({
      spinShow: true
    });
    if(this.data.userInfo.phone==null||this.data.userInfo.phone==""){
      this.setData({
        showLogin1: true
      })
    }
    this.indexLoad()
    app.formPost('/api/wx/student/user/current', null).then(res => {
      if (res.code == 1) {
        app.globalData.userInfo = res.response
      }
    }).catch(e => {
      app.message(e, 'error')
    })
  },
  goto(e) {
    const url=e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  gridchange: function (e) {
    this.setData({
      gridCol: e.detail.value
    });
  },
  gridswitch: function (e) {
    this.setData({
      gridBorder: e.detail.value
    });
  },
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '在线考试exam',
      path: '/page/index/index'
    }
  },
  onShareTimeline: function (t) {
    return {
      title: "在线考试exam",
      query: "",
      imageUrl: ""
    }
  },
  onPullDownRefresh() {
    this.setData({
      spinShow: true
    });
    if (!this.loading) {
      this.indexLoad()
      wx.showToast({
        title: '刷新成功',
        icon: "none"
      })
    }
  },
  indexLoad: function () {
    let _this = this
    app.formPost('/api/wx/student/dashboard/index', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          fixedPaper: res.response.fixedPaper,
          timeLimitPaper: res.response.timeLimitPaper,
          pushPaper: res.response.pushPaper,
          msgList: res.response.msgList,
          fixedPaperCount: res.response.fixedPaper.length,
          timeLimitPaperCount: res.response.timeLimitPaper.length
        });
        app.globalData.fixedPaper= res.response.fixedPaper
        app.globalData.timeLimitPaper= res.response.timeLimitPaper
        app.globalData.pushPaper= res.response.pushPaper
        app.globalData.fixedPaperCount= res.response.fixedPaper.length,
        app.globalData.timeLimitPaperCount= res.response.timeLimitPaper.length
        if( res.response.pushPaper!=null){
          _this.setData({
            pushPaperCount: res.response.pushPaper.length
          })
          app.globalData.pushPaperCount= res.response.pushPaper.length
        }
       
       
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
    })

    app.formPost('/api/wx/student/dashboard/task', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          taskList: res.response,
          taskPaperCount: res.response.length
        });
        app.globalData.taskList= res.response
        app.globalData.taskPaperCount= res.response.length
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
    })
    // 每日诗词
    jinrishici.load(result => {
      app.globalData.jinrishici1 = result.data.content
      // 下面是处理逻辑示例
      _this.setData({
        "jinrishici": result.data.content,
        shici: app.globalData.jinrishici1
      });

      console.log(app.globalData.jinrishici1)
    })
  },


})