const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    latitude: 30.562474,
    longitude: 114.351725,
    markers: []
  },
  onLoad: function (e) {
    let _this = this
    var examPaperId = e.examPaperId
    if (null != examPaperId) {
      app.formPost('/api/wx/student/exampaper/answer/addressList'+'/'+examPaperId, null).then(res => {
        console.log(res)
        if (res.code === 1) {
          console.log(res.code)
          _this.setData({
            markers: res.response
          });
          console.log(res.code)
        }
      }).catch(e => {
        app.message(e, 'error')
      })
    }
    else {
      app.formPost('/api/wx/student/exampaper/answer/address', null).then(res => {
        if (res.code === 1) {
          _this.setData({
            markers: res.response
          });
        }
      }).catch(e => {
        app.message(e, 'error')
      })
    }
  },
  myLocation: function() {
    var _this = this
    wx.getLocation({
      type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标  
      success: function(res) {
        _this.setData({
          longitude: res.longitude,
          latitude: res.latitude
        })
      }
    })
  },
  onReady: function (e) {
    this.mapCtx = wx.createMapContext('myMap1',this)
  },
})