let app = getApp()
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    spinShow: false,
    loadMoreLoad: false,
    loadMoreTip: '暂无数据',
    queryParam: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      paperName:""
    },
    tableData: [],
    total: 1
  },
  onLoad: function(options) {
    this.setData({
      spinShow: true
    });
    this.search(true)
  },
  onPullDownRefresh() {
    this.setData({
      spinShow: true
    });
    if (!this.loading) {
      this.setData({
        ['queryParam.pageIndex']: 1
      });
      this.search(true)
    }
  },
  onReachBottom() {
    if (!this.loading && this.data.queryParam.pageIndex < this.data.total) {
      this.setData({
        loadMoreLoad: true,
        loadMoreTip: '正在加载'
      });
      this.setData({
        ['queryParam.pageIndex']: this.data.queryParam.pageIndex + 1
      });
      this.search(false)
    }
  },
  searchValueOnChange: function (e) {
    this.setData({
      queryParam: {
        pageIndex: 1,
        pageSize: app.globalData.pageSize,
        paperName:e.detail.value
      },
    })
  },
          //页面滚动执行方式
          onPageScroll(e) {
            this.setData({
              scrollTop: e.scrollTop
            })
          },
  search: function(override) {
    let _this = this
    app.formPost('/api/wx/student/exampaper/answer/pageList', _this.data.queryParam)
      .then(res => {
        _this.setData({
          spinShow: false
        });
        wx.stopPullDownRefresh()
        if (res.code === 1) {
          const re = res.response
          _this.setData({
            ['queryParam.pageIndex']: re.pageNum,
            tableData: override ? re.list : _this.data.tableData.concat(re.list),
            total: re.pages
          });
          if (re.pageNum >= re.pages) {
            _this.setData({
              loadMoreLoad: false,
              loadMoreTip: '暂无数据'
            });
          }
        }
      }).catch(e => {
        _this.setData({
          spinShow: false
        });
        app.message(e, 'error')
      })
  }
})