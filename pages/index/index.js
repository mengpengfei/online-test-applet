const app = getApp();
let interstitialAd = null;
Page({
  data: {
    scrollTop: 0,
    advantageList: [{
      className: "type1",
      title: "考试中心",
      content: "轻松开启考试"
    }, {
      className: "type2",
      title: "学习中心",
      content: "便捷获取资料"
    }, {
      className: "type3",
      title: "便捷工具",
      content: "办公生活常见工具"
    }, {
      className: "type4",
      title: "服务中心",
      content: "工作生活的百科全书"
    }],
    height: '100',
    color: '#0081ef',
    bottom: 100,
    bgColor2: "#5677fc",
    btnList: [{
      bgColor: "#16C2C2",
      //名称
      text: "搜索",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }, {
      bgColor: "#64B532",
      //名称
      text: "关于",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }, {
      bgColor: "#FFA000",
      //名称
      text: "通知",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }],
    showwhat: '',
    showModalStatus: false,
    animationData: {},
    son_menu_of_title: '',
    bodylock: '',
    showLogin1: false,
    importance_show: false,
    bgColor1: '#f37b1d',
    color1: '#39b54a',
    showLogin: false,
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#e54d42', //文字图标颜色
    color1: '#9a9a9a', //文字图标颜色
    color3: '#fff',
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    cardCur: 0,
    spinShow: false,
    fixedPaperCount: 0,
    taskPaperCount: 0,
    timeLimitPaperCount: 0,
    pushPaperCount: 0,
    indexNumber: "",
    fixedPaper: [],
    pushPaper: [],
    timeLimitPaper: [],
    taskList: [],
    msgList: [],
    userInfo: null,
    greetOne: null,
    inportance_notice: [],
    queryParam: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize
    },
    message: 0,
    version: app.globalData.version
  },
  pageLifetimes: {
    show() {
      if (typeof this.getTabBar === 'function' && this.getTabBar()) {
        this.getTabBar().setData({
          selected: 0
        })
      }
    }
  },
  gongneng(e) {
// 在适合的场景显示插屏广告
if (interstitialAd) {
  interstitialAd.show().catch((err) => {
    console.error(err)
  })
}
    const index = e.currentTarget.dataset.id;
    switch (index) {
      case -1:
        break;
      case 0:
        wx.navigateTo({
          url: '/packageF/welcome/welcome',
        })
        break;
      case 1:
        wx.navigateTo({
          url: '/packageG/welcome/welcome',
        })
        break;
      case 2:
        wx.navigateTo({
          url: '/packageB/welcome/welcome',
        })
        break;
      case 3:
        wx.navigateTo({
          url: '/packageH/welcome/welcome',
        })
        break;
      default:
        break;
    }

  },
  onClick(e) {
    let index = e.detail.index
    switch (index) {
      case -1:
        break;
      case 0:
        wx.navigateTo({
          url: "/packageH/search/search"
        })
        break;
      case 1:
        wx.navigateTo({
          url: "/pages/my/aboutus/aboutus"
        })
        break;
      case 2:
        wx.navigateTo({
          url: "/pages/notice/notice"
        })
        break;
      default:
        break;
    }
  },
  onLoad: function () {
// 在页面onLoad回调事件中创建插屏广告实例
if (wx.createInterstitialAd) {
  interstitialAd = wx.createInterstitialAd({
    adUnitId: 'adunit-d776be13e5349dcb'
  })
  interstitialAd.onLoad(() => {})
  interstitialAd.onError((err) => {})
  interstitialAd.onClose(() => {})
}
    this.setData({
      spinShow: true
    });
    this.indexLoad()
    this.Get_time()
    this.get_notice()
    app.formPost('/api/wx/student/user/current', null).then(res => {
      if (res.code == 1) {
        app.globalData.userInfo = res.response
        this.setData({
          userInfo: res.response
        });
        if (this.data.userInfo.phone == null || this.data.userInfo.phone == "") {
          this.setData({
            showLogin1: true
          })
        }
      }
    }).catch(e => {
      app.message(e, 'error')
    })

  },
  get_notice() {
    var that = this
    app.formPost('/api/wx/student/importance/pageList', that.data.queryParam).then(res => {
      if (res.code == 1) {
        const randomKey = wx.getStorageSync('randomKey')
        // app.globalData.inportance_notice= res.response.list
        this.setData({
          indexNumber: res.response.list[0].id
        })
        app.globalData.inportance_notice = res.response.list
        if (randomKey != this.data.indexNumber) {
          that.setData({
            importance_show: true,
            inportance_notice: res.response.list[0]
          })
        }
      }
    })
  },
  know_notice() {
    var that = this
    wx.setStorageSync('randomKey', this.data.indexNumber);
    that.setData({
      importance_show: false
    })
  },
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  Get_time() {
    var that = this
    /* 加载时间日期 */
    var myDate = new Date();
    var month = myDate.getMonth() + 1
    var date = month + "-" + myDate.getDate();
    var h = myDate.getHours(); //获取当前小时数(0-23)
    if (h < 10) {
      h = "0" + String(h)
    }
    var greet = '';
    if (h < 6) {
      greet = "哎，失眠了"
    } else if (h < 9) {
      greet = "Hi，早上好吖"
    } else if (h < 12) {
      greet = "Hi，中午好吖"
    } else if (h < 14) {
      greet = "Hi，下午好吖"
    } else if (h < 17) {
      greet = "Hi，快下班吖"
    } else if (h < 21) {
      greet = "Hi，早点休息吖"
    } else if (h < 23) {
      greet = "Hi，洗洗睡吖"
    } else if (h = 23) {
      greet = "Hi，晚安吖"
    }
    this.setData({
      greetOne: greet
    });
  },
  indexLoad: function () {
    let _this = this
    _this.search(),
      app.formPost('/api/wx/student/dashboard/index', null).then(res => {
        _this.setData({
          spinShow: false
        });
        wx.stopPullDownRefresh()
        if (res.code === 1) {
          _this.setData({
            fixedPaper: res.response.fixedPaper,
            timeLimitPaper: res.response.timeLimitPaper,
            pushPaper: res.response.pushPaper,
            msgList: res.response.msgList,
            fixedPaperCount: res.response.fixedPaper.length,
            timeLimitPaperCount: res.response.timeLimitPaper.length,
          });
          app.globalData.examPaperCount = res.response.examPaperCount
          app.globalData.questionCount = res.response.questionCount
          app.globalData.doExamPaperCount = res.response.doExamPaperCount
          app.globalData.doQuestionCount = res.response.doQuestionCount
          app.globalData.fixedPaper = res.response.fixedPaper
          app.globalData.timeLimitPaper = res.response.timeLimitPaper
          app.globalData.pushPaper = res.response.pushPaper
          app.globalData.fixedPaperCount = res.response.fixedPaper.length,
            app.globalData.timeLimitPaperCount = res.response.timeLimitPaper.length
          if (res.response.pushPaper != null) {
            _this.setData({
              pushPaperCount: res.response.pushPaper.length
            })
            app.globalData.pushPaperCount = res.response.pushPaper.length
          }
        }
      }).catch(e => {
        _this.setData({
          spinShow: false
        });
        // app.message(e, 'error')
      })

    app.formPost('/api/wx/student/dashboard/task', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          taskList: res.response,
          taskPaperCount: res.response.length
        });
        app.globalData.taskList = res.response
        app.globalData.taskPaperCount = res.response.length
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      // app.message(e, 'error')
    })
  },

  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '在线考试exam',
      path: '/page/index/index'
    }
  },
  onShareTimeline: function (t) {
    return {
      title: "在线考试exam",
      query: "",
      imageUrl: ""
    }
  },
  onPullDownRefresh() {
    this.setData({
      spinShow: true
    });
    if (!this.loading) {
      this.indexLoad()
      wx.showToast({
        title: '刷新成功',
        icon: "none"
      })
    }
  },
  navProcess10(e) {
    wx.navigateTo({
      url: '/pages/portrait/portrait',
    });
  },
  navProcess11(e) {
    wx.navigateTo({
      url: '/packageG/mengxin/home',
    });
  },
  navProcess12(e) {
    if (!wx.getStorageSync('hasUserInfo')) {
      wx.getUserProfile({
        desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          console.log(res)
          wx.setStorageSync('userInfo', res.userInfo)
          wx.setStorageSync('nickName', res.userInfo.nickName)
          wx.setStorageSync('avatarUrl', res.userInfo.avatarUrl)
          wx.setStorageSync('hasUserInfo', true)
        }
      })
    }
    wx.navigateTo({
      url: '/packageC/friend/friend',
    });
  },
  navProcess13(e) {
    wx.navigateTo({
      url: '/pages/new/new',
    });
  },
  changyong(e) {
    wx.reLaunch({
      url: '/pages/home/index',
    })
  },

  search: function (override) {
    let _this = this
    app.formPost('/api/wx/student/user/message/unreadCount', null).then(res => {
      if (res.code === 1) {
        const re = res.response
        _this.setData({
          message: re
        });
      }
    })
  },

  // 跳转到搜索
  navSearch(e) {
    wx.navigateTo({
      url: '/packageH/search/search',
    });
  },

})