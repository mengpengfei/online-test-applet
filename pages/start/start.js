const app = getApp();
Page({

      /**
       * 页面的初始数据
       */
      data: {
            count: 3,
            bgurl: "https://www.dabenben.cn:9001/dma/startBg.jpg",
            version: app.globalData.version
      },
      onLoad() {
            const isFir = wx.getStorageSync('isFirst')
            if (!isFir) {
                  wx.redirectTo({
                        url: '/pages/onboarding/onboarding',
                  })
            } else {
                  this.countDown();
            }
      },
      go() {
            clearInterval(interval);
            wx.redirectTo({
                  url: '/pages/NewWelcome/loadPage',
            })
            // wx.switchTab({
            //       url: '/pages/NewWelcome/index',
            // })
      },
      countDown: function () {
            let that = this;
            let total = 3;
            this.interval = setInterval(function () {
                  total > 0 && (total--, that.setData({
                        count: total
                  })), 0 === total && (that.setData({
                              count: total
                        }),
                        wx.navigateTo({
                              url: '/pages/NewWelcome/loadPage',
                        }),
                        clearInterval(that.interval));
            }, 1e3);
      },
})