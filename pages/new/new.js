
const app = getApp()
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    isLoading:false,
    queryParam: {
      pageIndex: "1",
      pageSize: "5",
      title: ""
    },
  },
  onLoad() {
    this.setData({
      isLoading:true
    })
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            bookList: res.response.list,
            isLoading:false
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
  },
  goToHook(e) {
    const id = e.currentTarget.dataset.id;
    wx.showModal({
      title: "积分信息提示",
      content:
        "点击查看需要消耗您的积分，" +
        "请确认后点击【我已知晓】。",
      confirmText: "我已知晓",
      confirmColor: "#4B6DE9",
      cancelText: "我拒绝",
      success: result => {
        if (!result.confirm) {
         wx.navigateBack({
           delta: 0,
         })
        } else {
          wx.navigateTo({
            url: '/packageG/process/process?id=' + id,
        });
        }
      }
    });

  },
  onShareAppMessage() {
    return {
      title: '最近更新'
    }
  }
})