const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    neirong: null,
    userInfo:null,
    queryParam: {
      pageIndex: "1",
      pageSize: "1000",
      title:""
    },
    showLogin: false,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    color1:'#9a9a9a',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    shuLiang:null,
  },
    //页面滚动执行方式
    onPageScroll(e) {
      this.setData({
        scrollTop: e.scrollTop
      })
    },
    onLoad(){
      this.setData({
        userInfo:app.globalData.userInfo
      })
    },
    navActivity(e) {
      const id = e.currentTarget.dataset.id;
      if (this.data.userInfo.userName == "student"||this.data.userInfo.userDept === 1) {
        this.setData({
          showLogin: true
        })
        return
      }
          wx.navigateTo({
            url: '../process/process?id=' + id,
        });
        
    },
  search: function () {
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          bookList: res.response.list,
          shuLiang:res.response.total
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
    that.setData({
      modalName: "Modal",
      neirong: url
    });
    wx.setClipboardData({
      data: url,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功,粘贴到浏览器访问',
              icon: 'none'
            })
          }
        })
      }
    })
  },
  bindSearchInput: function (e) {
    this.setData({
      'queryParam.title': e.detail.value
    });
    this.setData({
      queryParam: {
        pageIndex: 1,
        pageSize: app.globalData.pageSize,
        title: e.detail.value
      },
    })
    // this.search();
    // return e.detail.value;
  },
});