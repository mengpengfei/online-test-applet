var wxbarcode = require('../../../utils/qrcod');
var Utils = require('../../../utils/util'); //相对路径
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    name:'',
    userInfo:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo:app.globalData.userInfo
    })
    let erweima = app.globalData.userInfo.id;
    app.globalData.encParam = Utils.encrypt(erweima); //文件名.方法名  加密
    console.log("加密后"+app.globalData.encParam)
    wxbarcode.qrcode('qrcode', this,  app.globalData.encParam, 600, 350);
  },
  onShow(){
    // 登录凭证，假设name有即登录
    this.setData({
      name:wx.getStorageSync('name')
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})