var app = getApp();

Page({

      /**
       * 页面的初始数据
       */
      data: {
            StatusBar: app.globalData.StatusBar,
            CustomBar: app.globalData.CustomBar,
            weixin: "chen31362008",
            qq: "576752171",
            gzh: "大笨笨一号",
            phone: "18942109494",
            banner: "https://www.dabenben.cn:9001/dma/kefu.jpg"
      },
      onLoad() {

      },

      //复制
      copy(e) {
            wx.setClipboardData({
                  data: e.currentTarget.dataset.copy,
                  success: res => {
                        wx.showToast({
                              title: '复制' + e.currentTarget.dataset.name+'成功',
                              icon: 'success',
                              duration: 1000,
                        })
                  }
            })
      },
      //电话拨打
      phone(e) {
            wx.makePhoneCall({
                  phoneNumber: e.currentTarget.dataset.phone
            })
      },
})