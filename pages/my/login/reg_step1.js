
const app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
    StatusBar: app.globalData.StatusBar,
		CustomBar: app.globalData.CustomBar,
		showAuth: true,
		showform: true,
		users:[],
		userInfo: null
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: async function (options) { 

 
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {},

	bindGetPhoneNumber: async function (e) {
		// await RegBiz.registerStep1(e);
		console.log(e)
	},
    // 获取输入框数据
    InputData: function (e) {
			console.log(e, e.currentTarget.id, e.detail.value)
			let users = this.data.users
			let id = e.currentTarget.id
			let value = e.detail.value
			users[id] = value
			this.setData({
					users
			})
	},

	// 提交注册信息
	SubmitRegister(e) {
			// 保存

			let users = this.data.users
			let name = users['name']
			let phone = users['phone']
			// 保存到数据库
			if(name==phone&&name!=null){
					wx.showLoading({
							mask: true,
							title: '正在保存...',
					})
					app.globalData.userInfo.phone=name
					this.setData({
							userInfo:app.globalData.userInfo,
							'userInfo.phone':name,
							'userInfo.imagePath':app.globalData.avatarUrl,
							'userInfo.age':0,
					})
					app.formPost('/api/wx/student/user/update', this.data.userInfo)
					.then(res => {
						if (res.code == 1) {
							wx.navigateTo({
								url: '/pages/my/login/reg_step2',
							})
						} else {
						}
						wx.hideLoading()
					}).catch(e => {
						wx.hideLoading()
					})
			}else{
					wx.showModal({
							title: '提示',
							content: '两次输入手机号码不一致！'
						})
						return
			}

	},

	// 返回首页
	onClickLeft() {
			wx.switchTab({
					url: '../home/home',
			})
	},

})