const app = getApp()
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    logList: [],
    len: 0,
    total:0,
    queryParam: {
      pageIndex: "1",
      pageSize: "10",
      content: "查询了"
    }
  },
  onLoad: function(options) {
    this.search();
  },
  nextPage:function(){
    if((parseInt(this.data.queryParam.pageIndex)+10)<=this.data.total){
      this.setData({
        spinShow: true,
        'queryParam.pageIndex': parseInt(this.data.queryParam.pageIndex)+10
      });
    }else{
      this.setData({
        spinShow: true,
        'queryParam.pageIndex': parseInt(this.data.queryParam.pageIndex)
      });
    }
      this.search();
  },
  lastPage:function(){
    if(parseInt(this.data.queryParam.pageIndex)>=10){
    this.setData({
      spinShow: true,
      'queryParam.pageIndex': parseInt(this.data.queryParam.pageIndex)-10
    });}else{
      this.setData({
        spinShow: true,
        'queryParam.pageIndex': 1
      });
    }
      this.search();
  },
  search: function() {
    let _this = this
    _this.setData({
      spinShow: true
    });
      app.formPost('/api/wx/student/user/log/pageList', _this.data.queryParam).then(res => {
      if (res.code === 1) {
        _this.setData({
          logList: res.response.list,
          len: res.response.length,
          total:res.response.total,
          spinShow: false
        });
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      app.message(e, 'error')
    })
  }
})