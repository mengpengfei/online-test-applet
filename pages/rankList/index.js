// pages/rankList/index.js
//index.js
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rankList:[],
    loading: true,
    index:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
    var examPaperId = options.examPaperId
    app.formPost('/api/wx/student/exampaper/answer/rankListOne'+'/'+examPaperId, null).then(res => {
      console.log(res)
    this.setData({
      rankList:res.response,
      loading:false
       })
    })    
  }
})