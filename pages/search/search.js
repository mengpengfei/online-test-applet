// pages/course/search.js
var app = getApp();

Page({
  data: {
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    header: {
      defaultValue: '',
      inputValue: '',
      help_status: false,
      help_class_status: false
    },
    main: {
      mainDisplay: true, // main 显示的变化标识
      list: []
    },
    pageType: 'teacher',
    spinShow: false,
    loadMoreLoad: false,
    loadMoreTip: '暂无数据',
    queryParam: {
      pageIndex: 1,
      pageSize: app.globalData.pageSize,
      paperName: '',
    },
    tableData: [],
    total: 1
  },
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  bindClearSearchTap: function (e) {
    this.setData({
      'main.mainDisplay': true,
      'main.total': 0,
      'queryParam.paperName': ''
    });
  },
  bindSearchInput: function (e) {
    if (this.data.main.mainDisplay != false) {
      this.setData({
        'main.mainDisplay': !this.data.main.mainDisplay
      });
    }
    this.setData({
      'queryParam.paperName': e.detail.value
    });
    this.setData({
      queryParam: {
        pageIndex: 1,
        pageSize: app.globalData.pageSize,
        paperName: e.detail.value
      },
    })
    this.search();
    return e.detail.value;
  },

  bindSearchInputClass: function (e) {
    if (this.data.main.mainDisplay != false) {
      this.setData({
        'main.mainDisplay': !this.data.main.mainDisplay
      });
    }
    this.setData({
      'queryParam.paperName': e.detail.value
    });
    this.searchClass();
    return e.detail.value;
  },

  // 点击搜索教师
  bindConfirmSearchTap: function () {
    this.search();
  },
  // 点击搜索班级
  bindConfirmSearchTapClass: function () {
    this.searchClass();
  },
  // 搜索教师
  search: function (key) {
    if (this.data.queryParam.paperName.length < 1) {
      wx.showToast({
        title: '请输入试卷名称',
        image: '../../images/info.png'
      });
      return;
    }
    let _this = this
    _this.setData({
      tableData: []
    });
    app.formPost('/api/wx/student/exampaper/answer/pageList', _this.data.queryParam)
      .then(res => {
        if (res.code === 1) {
          const re = res.response
          _this.setData({
            ['queryParam.pageIndex']: re.pageNum,
            tableData: key ? re.list : _this.data.tableData.concat(re.list),
            total: re.pages
          });
          if (re.pageNum >= re.pages) {
            _this.setData({
              loadMoreLoad: false,
              loadMoreTip: '暂无数据'
            });
          }
        }
      }).catch(e => {
        _this.setData({
          spinShow: false
        });
        app.message(e, 'error')
      })

  },
  onLoad: function (options) {},

  tapHelp: function (e) {
    if (e.target.id == 'help') {
      this.hideHelp();
    }
  },
  showHelp: function (e) {
    // console.log(e)
    var that = this;
    that.setData({
      'header.help_status': true
    });
  },
  showHelpClass: function (e) {
    // console.log(e)
    var that = this;
    that.setData({
      'header.help_class_status': true
    });
  },
  hideHelp: function (e) {
    this.setData({
      'header.help_status': false,
      'header.help_class_status': false
    });
  },
});