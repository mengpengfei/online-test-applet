const app = getApp()
Page({
  // options: {
  //   addGlobalClass: true,
  // },
  data: {
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor1:'blue',//背景色
    color:'#fff',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    bgColor:'#f4f5f7',
    height:'30',
    top:0,
    bottom:0,
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    elements: [{
        title: '调研',
        name: 'survey',
        color: 'cyan',
        icon: 'newsfill'
      },
      {
        title: '设计院',
        name: 'designing',
        color: 'blue',
        icon: 'colorlens'
      },
      {
        title: '方案',
        name: 'scheme',
        color: 'purple',
        icon: 'font'
      },
      {
        title: '交流会 ',
        name: 'meeting',
        color: 'mauve',
        icon: 'icon'
      },
      {
        title: '招投标',
        name: 'bidding',
        color: 'pink',
        icon: 'btn'
      },
      {
        title: '基础知识',
        name: 'knowledge',
        color: 'brown',
        icon: 'tagfill'
      },
    ],
    scrollTop: 0, 
        listColor: [], 
        list: [{
            title: '在线询价', text: '支持根据方案在线匹配价格', name: '@大笨笨一号', icon: 'list', appId: 'wxb7c9e6814c98ac52'
        }, {
            title: '个人简历', text: '微信小程序版本个人简历', name: '@大笨笨一号', icon: 'list1', appId: ''
        }, {
            title: '文档助手', text: '文档助手小程序，便捷分享', name: '@大笨笨一号', icon: 'top', appId: 'wx90532cf6d92591d9'
        }, {
          title: '婚礼邀请函', text: '婚礼邀请函小程序', name: '@大笨笨一号', icon: 'code', appId: ''
      }]
  },
  
  onLoad() {
    let list = this.data.list, 
    color = [];
    for (let i = 0; i < list.length; i++) {
        let val = this.getColor();
        color.push(val)
    }
    this.setData({
        listColor: color
    })
},
//随机生成库内颜色名
getColor() {
    let colorArr = ['yellow', 'orange', 'red', 'pink', 'mauve', 'purple', 'blue', 'cyan', 'green', 'olive', 'grey', 'brown'];
    return colorArr[Math.floor(Math.random() * colorArr.length)]
},
tapToUrl(e) {
  if(e.currentTarget.dataset.appid==''){
    wx.showModal({
      title: '小程序暂时下线，敬请期待！',
    })
  }
  wx.navigateToMiniProgram({
    appId: e.currentTarget.dataset.appid,
  })
},
// 监听用户滑动页面事件。
onPageScroll(e) {
    // 注意：请只在需要的时候才在 page 中定义此方法，不要定义空方法。以减少不必要的事件派发对渲染层-逻辑层通信的影响。
    // 注意：请避免在 onPageScroll 中过于频繁的执行 setData 等引起逻辑层-渲染层通信的操作。尤其是每次传输大量数据，会影响通信耗时。
    // https://developers.weixin.qq.com/miniprogram/dev/reference/api/Page.html#onPageScroll-Object-object
    this.setData({
        scrollTop: e.scrollTop
    })
},

})