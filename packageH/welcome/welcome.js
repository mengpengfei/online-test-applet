
const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    version: app.globalData.version,
    imgs: [
      "https://cdns.qdu.life/class/onboarding_1.png",
      "https://cdns.qdu.life/class/onboarding_2.png",
      "https://cdns.qdu.life/class/onboarding_last.png",
      "https://cdns.qdu.life/class/onboarding_last.png",
    ],
    currentSwiper: 0,
  },

  swiperChange: function (e) {
    this.setData({
      currentSwiper: e.detail.current
    })
  },
  onLoad(){

  },
  start(){
    wx.navigateTo({
      url: '/packageH/home/index',
    })
  }
})
