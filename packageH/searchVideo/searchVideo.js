const app = getApp();
Page({
  data: {
    opacity: '.3', //字体透明度
    color2: '#081EF', //字体颜色
    number: 6, //水印数量
    deg: '-45', //旋转角度
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    neirong: null,
    userInfo: null,
    isLoading: false,
    queryParam: {
      pageIndex: "1",
      pageSize: "10",
      dept:""
    },
    showLogin: false,
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    color1: '#9a9a9a', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    shuLiang: null,
    hislist: [],
    bookList:[],
    status:'loading',
    value:""
  },
  loadmore(){
    setTimeout(() => {
      this.setData({
        'queryParam.pageSize':this.data.queryParam.pageSize+10,
        status:'loadmore'
      })
      this.search()
    })
  },
     // 触底加载
     onReachBottom(){
      this.setData({
        status:'loading'
      })
      // 模拟加载
    
        this.loadmore()
      
    },
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  onLoad(options) {
    this.setData({
      userInfo: app.globalData.userInfo
    });
    this.gethis();
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    wx.showModal({
      title: "积分信息提示",
      content:
        "点击查看需要消耗您的积分，" +
        "请确认后点击【我已知晓】。",
      confirmText: "我已知晓",
      confirmColor: "#4B6DE9",
      cancelText: "我拒绝",
      success: result => {
        if (!result.confirm) {
         wx.navigateBack({
           delta: 0,
         })
        } else {
          wx.navigateTo({
            url: '/packageG/shipin/shipin?id=' + id,
          });
        }
      }
    }); 

  },
  search: function () {
    this.setData({
      isLoading:true
    })
    // this.history(this.data.queryParam.title)
    app.formPost('/api/wx/student/video/pageList', this.data.queryParam)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            bookList: res.response.list,
            shuLiang: res.response.total,
            spinShow:false,
            isLoading:false
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  //添加到搜索历史
  history(key) {
    let that = this;
    wx.getStorage({
      key: 'history',
      success(res) {
        let oldarr = JSON.parse(res.data); //字符串转数组
        let newa = [key]; //对象转为数组
        let newarr = JSON.stringify(newa.concat(oldarr)); //连接数组\转字符串
        wx.setStorage({
          key: 'history',
          data: newarr,
        })
      },
      fail(res) {
        //第一次打开时获取为null
        let newa = [key]; //对象转为数组
        var newarr = JSON.stringify(newa); //数组转字符串
        wx.setStorage({
          key: 'history',
          data: newarr,
        })
      }
    });
  },
  clearAll(){
    wx.removeStorageSync('history')
    this.setData({
      hislist: ""
    })
  },
  //获取本地记录
  gethis() {
    let that = this;
    wx.getStorage({
      key: 'history',
      success: function (res) {
        let hislist = JSON.parse(res.data);
        //限制长度
        if (hislist.length > 10) {
          hislist.length = 10
        }
        that.setData({
          hislist: hislist
        })
      },
    })
  },
  bindSearchInput: function (e) {
    this.setData({
      queryParam: {
        pageIndex: 1,
        pageSize: app.globalData.pageSize,
        dept: e.detail.value
      },
      value:e.detail.value
    })

    // this.search();
    // return e.detail.value;
  },
});