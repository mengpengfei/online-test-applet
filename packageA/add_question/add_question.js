// miniprogram/pages/add/addt.js
const app = getApp()
Page({
  data: {
    routers: [
      {
        name: '单选',
        url: '/packageA/add/add_radio/add_radio?index=-1',
        icon: '../add_question/images/check-circle.png',
        code: '10'
      },
      {
        name: '多选',
        url: '/packageA/add/add_checkbox/add_checkbox?index=-1',
        icon: '../add_question/images/check-square.png',
        code: '11'
      },
      {
        name: '电话',
        url: '/packageA/add/add_phone/add_phone?index=-1',
        icon: '../add_question/images/phone.png',
        code: '10'
      },
      {
        name: '问答',
        url: '/packageA/add/add_blank/add_blank?index=-1',
        icon: '../add_question/images/edit.png',
        code: '11'
      },
      {
        name: '打分评价',
        url: '/packageA/add/add_star/add_star?index=-1',
        icon: '../add_question/images/star.png',
        code: '10'
      },
      {
        name: '下拉选择',
        url: '/packageA/add/add_list/add_list?index=-1',
        icon: '../add_question/images/detail.png',
        code: '11'
      },
      {
        name: '图片',
        url: '/packageA/add/add_id/add_id?index=-1',
        icon: '../add_question/images/id-card.png',
        code: '10'
      },
      {
        name: '日期',
        url: '/packageA/add/add_date/add_date?index=-1',
        icon: '../add_question/images/calendar.png',
        code: '11'
      },
      {
        name: '邮箱',
        url: '/packageA/add/add_mail/add_mail?index=-1',
        icon: '../add_question/images/calendar.png',
        code: '10'
      }
    ]
  },

})
