//pages/login/login.js
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  protocol: function() {
    wx.navigateTo({
      url: '../../pages/user/about/about'
    })
  },
  back: function() {
    wx.reLaunch({
      url: '../../pages/index/index'
    })
  },
  goto: function() {
    wx.redirectTo({
      url: '../../packageA/index/index',
    })
  },
  /**
   *从云端获取资料
   *如果没有获取到则尝试新建用户资料
   */


  onLoad: function (options) {

    let _this = this;
    //查看是否授权
    },

  /**
  *从云端获取资料
  *如果没有获取到则尝试新建用户资料
  */

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})