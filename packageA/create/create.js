// pages/create/create.js
Page({
  //注意：本页面不能被返回
  data: {

  },
  
  //创建新文件
  createNew: function(){
    wx.redirectTo({
      url: '/packageA/title_abstract/title_abstract?index=-1',
    })
    //     wx.showModal({
    //   title: '提示',
    //   content: '此功能正在开发中，敬请期待！'
    // })
  },

  //复制已有模板
  createOld: function(){
    //跳转至模板选择页面
    // wx.redirectTo({
    //   url: '../choose/choose',
    // })
    wx.showModal({
      title: '提示',
      content: '此功能正在开发中，敬请期待！'
    })
  },

  createBaoming: function () {
    // wx.redirectTo({
    //   url: '/pages/editor/editor?index=3',
    // })
    wx.showModal({
      title: '提示',
      content: '此功能正在开发中，敬请期待！'
    })
  },

  createManyi: function () {
    // wx.redirectTo({
    //   url: '/pages/editor/editor?index=4',
    // })
    wx.showModal({
      title: '提示',
      content: '此功能正在开发中，敬请期待！'
    })
  },

  createToupiao: function () {
    // wx.redirectTo({
    //   url: '/pages/editor/editor?index=5',
    // })
    wx.showModal({
      title: '提示',
      content: '此功能正在开发中，敬请期待！'
    })
  },

  createTest: function () {
    // wx.redirectTo({
    //   url: '/pages/editor/editor?index=6',
    // })
    wx.showModal({
      title: '提示',
      content: '此功能正在开发中，敬请期待！'
    })
  },


  //转发
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '欢迎使用在线考试',
      path: '/packageA/index/index',
      success: function (res) {
        wx.showToast({
          title: '转发成功',
          icon: "none",
          duration: 1500
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
          icon: "none",
          duration: 1500
        })
      }
    }
  }
})